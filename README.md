# py-mocp

## Description
This Python module implements unix socket client for MOC Player.
Also it containts a simple script to display OSD notifications on track change.

## Usage
mocp-notify.py is a simple python script that shows a popup with the current track for MOC Player.
```
user@localhost:~$ pip3 install py-mocp
user@localhost:~$ mocp-notify.py
```
